from src.service.parse_options import ParseOptions
from src.domain.entity.connection import ConnectionFile
import logging

logger = logging.getLogger(__name__)


def main():
    parser = ParseOptions()
    options = parser.get_options()
    cnn = ConnectionFile()
    connection_list = cnn.host_connections_to(options.file_name, options.time_init, options.time_end, options.host_name)
    cnn.write_result(connection_list)


if __name__ == '__main__':
    main()
