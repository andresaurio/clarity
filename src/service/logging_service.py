import logging
from src.domain.model.singleton import SingletonType


class Logging(object, metaclass=SingletonType):
    __logger = None

    def __init__(self):
        self.__logger = logging.getLogger("app")
        self.__logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s > [%(levelname)s | %(filename)s:%(lineno)s] > "%(message)s"',
                                      datefmt='%Y-%m-%d %H:%M:%S')
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(formatter)
        self.__logger.addHandler(stream_handler)

    @staticmethod
    def get_instance():
        """ Static access method. """
        if Logging.__logger is None:
            return Logging().__logger
        return Logging.__logger
