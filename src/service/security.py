import jwt
from jwt import PyJWTError
import os
from pydantic import BaseModel
from fastapi.exceptions import HTTPException
from fastapi import status
from datetime import datetime, timedelta


class Security:
    def __init__(self):
        self.expire_token_minutes = 30
        self.algorithm = "HS256"
        self.secret_key = str(os.getenv('SECRET_KEY'))
        self.token_password = str(os.getenv('TOKEN_PASSWORD'))
        self.credentials_exception = self.get_credentials_exception()

    def verify_api_password(self, user: str, password: str):
        if None not in (password, user) and password == self.token_password:
            d = {'user': user, 'password': password}
            token = self.__create_access_token(d)
            return token.decode('UTF-8')
        raise self.credentials_exception

    def __create_access_token(self, data: dict):
        to_encode = data.copy()
        expire = datetime.utcnow() + timedelta(minutes=self.expire_token_minutes)
        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(to_encode, self.secret_key)
        return encoded_jwt

    class ResponseModelToken(BaseModel):
        access_token: str
        token_type: str

    @staticmethod
    def get_credentials_exception():
        return HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

    def jwt_decode(self, token: str) -> bool:
        try:
            jwt.decode(token, self.secret_key, algorithms=self.algorithm)
        except PyJWTError:
            return False
        return True
