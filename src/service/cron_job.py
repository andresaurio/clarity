import os
from typing import List
from src.service.config import Config
from src.domain.entity.connection import ConnectionFile
import time
from src.domain.entity.connection import Connection


class JobTime:
    def __init__(self, minutes: int):
        self.time_end = int(time.time())
        self.time_ini = self.time_end - (int(minutes) * 60)


class CronJob:

    def __init__(self):
        self.__cfg = Config.get_instance()
        self.__file_list: List[str] = self.local_file_list()

    def host_names_connected(self, host_to: str, host_from: str, minutes: int):
        job_time = JobTime(minutes)
        result_host_to: List[Connection] = []
        result_host_from: List[Connection] = []
        cnn = ConnectionFile()

        for file in self.__file_list:
            cnn_list_to, cnn_list_from = cnn.host_connections_cron_job(
                file_name=file,
                init_time=job_time.time_ini,
                end_time=job_time.time_end,
                host_to=host_to,
                host_from=host_from)

            result_host_to.extend(cnn_list_to)
            result_host_from.extend(cnn_list_from)
        cnn.write_result(result_host_from)
        cnn.write_result(result_host_to)

    def local_file_list(self) -> List[str]:
        data_dir = self.__cfg.get('cfg', 'data_folder')
        return os.listdir(data_dir)
