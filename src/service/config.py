import os
import yaml
from os.path import join
from src.domain.model.singleton import SingletonType


class Config(metaclass=SingletonType):
    """
    This class is a singleton which contains the whole configuration of the project
    """
    __instance = None

    def __init__(self):
        """ Virtually private constructor.
        This class can be accessed on every module of the project with ne next syntax :
        Example::
        config = ConfigService.get_instance()
        """
        self.config = {}
        if Config.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            Config.__instance = self
            path = os.getenv('CONFIG_PATH')
            for file in os.listdir(path):
                if '.yml' in file:
                    f = open(join(path, file), 'r')
                    self.config[file.replace('.yml', '')] = yaml.load(f, Loader=yaml.FullLoader)
                    f.close()

    @staticmethod
    def get_instance():
        """ Static access method. """
        if Config.__instance is None:
            Config()
        return Config.__instance

    def __get_config(self):
        return self.config

    def verify_configuration(self):
        pass

    def get(self, section, option):
        return self.__get_config()[section][option]

    def get_dict(self, section):
        return self.__get_config()[section]
