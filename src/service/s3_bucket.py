import boto3
from botocore.exceptions import ClientError
from src.service.logging_service import Logging
from src.service.config import Config
from fastapi.exceptions import HTTPException
from fastapi import status


class S3:
    def __init__(self, bucket_name):
        self.bucket_name = bucket_name
        self.s3 = boto3.resource('s3')
        self.cfg = Config.get_instance()
        self.log_service = Logging().get_instance()

    def download(self, file, route):
        """
        Download the needed files for the prediction from the S3
        :param file: route of the files on the bucket
        :param route: route of the files on local
        :return:
        """
        try:
            self.s3.Bucket(self.bucket_name).download_file(file, (route + file))
            return True
        except ClientError as e:
            self.log_service.error(f"Error performing the download on the S3 client:  {e}")
            return False

    @staticmethod
    def get_file_exception(file_name):
        return HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Could not download {file_name}",
        )
