import argparse
import sys


class ParseOptions:
    def __init__(self):
        self.__parser = argparse.ArgumentParser(description='Process the input options')
        self.__add_file_argument()
        self.__add_time_end_argument()
        self.__add_time_init_argument()
        self.__add_host_name_argument()

    def __add_file_argument(self):
        self.__parser.add_argument('--file_name', metavar='F', type=str)

    def __add_time_init_argument(self):
        self.__parser.add_argument('--time_init', metavar='INI', type=int)

    def __add_time_end_argument(self):
        self.__parser.add_argument('--time_end', metavar='END', type=int)

    def __add_host_name_argument(self):
        self.__parser.add_argument('--host_name', metavar='H', type=str)

    def get_options(self, args=None):
        if args is None:
            args = sys.argv[1:]
        return self.__parser.parse_args()
