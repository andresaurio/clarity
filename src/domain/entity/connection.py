from src.service.config import Config
import os
from collections import defaultdict
from typing import List, Tuple
from datetime import datetime
import uuid
from src.service.logging_service import Logging
from pydantic import BaseModel


class Connection:
    """
    Object used to identify a connection easily.
    Cast some items to the proper type and override some useful methods
    in case we want to order the items somewhere.
    """

    def __init__(self, unix_time: int, host_from: str, host_to: str):
        """
        A basic connection is instantiated with the following variables.
        :param unix_time: the time of the connection : uni_timestamp is received
        :param host_from: host which realizes the connection
        :param host_to: host who receive the connection
        """
        self.unix_time: int = unix_time
        self.host_from: str = host_from
        self.host_to: str = host_to

    def __lt__(self, other):
        return self.unix_time < other.unix_time

    def __repr__(self):
        return f'{self.unix_time},{self.host_from},{self.host_to},\n'

    def __str__(self):
        return f'{self.unix_time},{self.host_from},{self.host_to},\n'

    def __eq__(self, other):
        return self.unix_time == other.unix_time and self.host_from == other.host_from and self.host_to == other.host_to

    def obj_dict(self, obj):
        return {
            'unix_time': self.unix_time,
            'host_from': self.host_from,
            'host_to': self.host_to
        }


class ConnectionFile:
    __file_index = {
        'unix_time': 0,
        'host_from': 1,
        'host_to': 2
    }
    __five_minutes_unix_time = 5 * 60

    def __init__(self):
        """
        Instantiates the object to process a connection file.
        It needs the configuration of the project for some folders route inside the container.
        Also the necessary data structures to store the results.

        Initially I thought a dictionary would be a nice data structure because we can have O(1)
        time complexity indexing a given host. Anyway we only receive one but If we wanted to
        know multiples connection list for multiple host we can index all of them faster.
        """
        self.cfg = Config.get_instance()
        self.connection_list: defaultdict[str] = defaultdict(list)
        self.log_service = Logging().get_instance()

    def __read_file(self, file_name: str):
        """
        Given a file_name parameter try to read the file inside de data folder, named on the configuration file.
        :param file_name:
        :return:
        """
        data_route = self.cfg.get('cfg', 'data_folder')
        file_route = f'{data_route}/{file_name}'
        if not os.path.isfile(file_route):
            self.log_service.error(msg=f'The file {file_name} does not exits on the {data_route} folder')
            raise Exception(f'The file {file_name} does not exits on the {data_route} folder')

        return open(file_route, 'r')

    def host_connections_to(self, file_name: str, init_time: int, end_time: int, host_name: str) -> List[Connection]:
        """
        Returns the list of the connections for the given host.
        :param file_name: file which is going to be iterated.
        :param init_time: start of the time interval
        :param end_time: end of the time interval
        :param host_name: the host for which we want to know the connections
        :return: the list of the connection for host_name

        I wanted to perform some optimizations in order to keep the time complexity fast and the
        memory usage low.

        -- First I don't iterate over the entire file, since It's "roughly" roughly sorted It doesn't worth
        order the file. The delay is 5 minutes maximum so I start to append connections to the list five minutes before
        the initial time, so I dont waste memory with registers that I won't use.
        Then I stop five minutes after the end time so I don't miss any possible value which is out of order.

        So the Big-O analysis will be:
        O(N) With N as the number of the registers between the initial time of the file ( not init_time )
        and end_time + 5 five minutes.

        Space analysis :
        O(C) with C as the number of registers between init_time - five minutes and end_time + five_minutes
        """
        self.log_service.info(msg=f'Performing execution of the host_connections_to for the file : {file_name}')
        file_descriptor = self.__read_file(file_name)

        # O(N)
        terminated = False
        while (row := file_descriptor.readline()) and not terminated:
            row = row.split()
            cnn = Connection(
                unix_time=int(row[self.__file_index.get('unix_time')]),
                host_from=row[self.__file_index.get('host_from')],
                host_to=row[self.__file_index.get('host_to')]
            )
            if cnn.unix_time > end_time + self.__five_minutes_unix_time:
                terminated = True
            elif self.__evaluate_connection_to(init_time, end_time, host_name, cnn):
                self.connection_list[row[self.__file_index.get('host_to')]].append(cnn)
        file_descriptor.close()
        return self.connection_list[host_name]

    @staticmethod
    def __evaluate_connection_to(init_time: int, end_time, host_to, cnn: Connection) -> bool:
        # Evaluates if the connection is on the correct time range.

        return int(init_time) <= cnn.unix_time \
               <= int(end_time) and cnn.host_to == host_to

    @staticmethod
    def __evaluate_connection_from(init_time: int, end_time, host_from, cnn: Connection) -> bool:
        # Evaluates if the connection is on the correct time range.
        return int(init_time) <= cnn.unix_time \
               <= int(end_time) and cnn.host_from == host_from

    def host_connections_cron_job(self, file_name: str, init_time: int, end_time: int, host_to: str, host_from: str) \
            -> Tuple[List[Connection], List[Connection]]:
        file_descriptor = self.__read_file(file_name)
        self.log_service.info(
            msg=f'Performing the execution of the cron functionality for host_to:{host_to} , host_from:{host_from}')
        terminated = False
        while (row := file_descriptor.readline()) and not terminated:
            row = row.split()
            cnn = Connection(
                unix_time=int(row[self.__file_index.get('unix_time')]),
                host_from=row[self.__file_index.get('host_from')],
                host_to=row[self.__file_index.get('host_to')]
            )
            if cnn.unix_time > end_time + self.__five_minutes_unix_time:
                terminated = True

            else:
                if self.__evaluate_connection_to(init_time, end_time, host_to, cnn):
                    self.connection_list[row[self.__file_index.get('host_to')]].append(cnn)
                if self.__evaluate_connection_from(init_time, end_time, host_from, cnn):
                    self.connection_list[row[self.__file_index.get('host_from')]].append(cnn)

        file_descriptor.close()
        return self.connection_list[host_to], self.connection_list[host_from]

    def write_result(self, result: List[Connection]):
        if len(result) == 0:
            self.log_service.info(msg='The result list is empty')

        self.log_service.info(msg='Writing results')
        today = datetime.now()
        time = today.strftime('%Y-%m-%dT%H:%M:%S')
        process_id = uuid.uuid1()
        file_descriptor = open(f"{self.cfg.get('cfg', 'result_folder')}/{process_id}_{time}.csv", 'w')
        for row in result:
            file_descriptor.write(str(row))
        file_descriptor.close()

    class ResponseModelConnection(BaseModel):
        class Connection(BaseModel):
            unix_time: int
            host_from: str
            host_to: str
        result: List[Connection]
