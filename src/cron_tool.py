import os
from src.service.cron_job import CronJob


def main():
    job = CronJob()
    job.host_names_connected(
        host_to=os.environ.get('HOST_TO'),
        host_from=os.environ.get('HOST_FROM'),
        minutes=os.environ.get('MINUTES')
    )


if __name__ == '__main__':
    main()
