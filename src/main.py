from fastapi import FastAPI, Depends
from src.service.logging_service import Logging
from src.service.security import Security
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer
from fastapi.middleware.cors import CORSMiddleware
from src.domain.entity.connection import ConnectionFile
from src.service.s3_bucket import S3
from src.service.config import Config
import json

cnn = ConnectionFile()
app = FastAPI()
log = Logging().get_instance()
security = Security()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl='/token')
config = Config.get_instance()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


@app.get('/alive', response_model=str)
def is_alive(token: str):
    if not security.jwt_decode(token):
        raise security.credentials_exception
    result = 'Hell'
    return result


@app.post('/token', response_model=security.ResponseModelToken)
def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    if not form_data.username or not form_data.password:
        raise security.credentials_exception
    access_token = security.verify_api_password(form_data.username, form_data.password)
    return {'access_token': access_token, 'token_type': 'bearer'}


@app.post('/connection', response_model=cnn.ResponseModelConnection)
def connection(file_name: str, time_init: int, time_end: int, host_name: str, token: str = Depends(oauth2_scheme)):
    if not security.jwt_decode(token):
        raise security.credentials_exception
    s3 = S3(config.get('cfg', 's3_bucket'))
    download_status = s3.download(file_name, config.get('cfg', 'data_folder'))
    if download_status:
        connection_list = cnn.host_connections_to(file_name, time_init, time_end, host_name)
        res = [x.obj_dict(x) for x in connection_list]
        return {'result': res}
    raise s3.get_file_exception(file_name)
