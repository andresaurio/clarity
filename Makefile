SHELL := /bin/bash
install:
	python -m pip install --upgrade pip
	pip3 install -r requirements.txt
	@echo "🚀 INSTALL DEPS ok"

testing:
	python -m unittest
	@echo "✔️ TESTS ok"

init:
	uvicorn src.main:app --reload --host ${APP_HOST} --port ${APP_PORT}