FROM python:3.8-slim-buster as clarity_test

RUN apt-get update && apt-get install -y --no-install-recommends \
    cron \
    nano \
    make \
    && rm -rf /var/lib/apt/lists/*

FROM clarity_test
ARG APP_FOLDER=/app/
WORKDIR ${APP_FOLDER}
RUN mkdir result
COPY ./requirements.txt ${APP_FOLDER}
COPY ./Makefile ${APP_FOLDER}
COPY ./src ${APP_FOLDER}src/
COPY ./config ${APP_FOLDER}config/
COPY ./test ${APP_FOLDER}test/
RUN make install


ENV CONFIG_PATH=/app/config/
ENV PYTHONPATH=$PYTHONPATH:/app/
ENV APP_PORT=8080
ENV APP_HOST=0.0.0.0

CMD uvicorn src.main:app --reload --host $APP_HOST --port $APP_PORT