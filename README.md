# General info 
##Folders :
- config : You can find here configuration of the project. Actually there's just a few local routes
inside the docker to reference them easily on the code.
- data : I assume you have this folder on the project root. It's where all the data files are being stored.
- log : Folder where the logs are placed, contains information about the execution, errors, etc..
- result: I save here the results of the requests. I assume you have this folder on the root.
- src : All the application code.
- Dockerfile : Docker image
- docker-compose-cron-job.yml : tool to perform the cron job.
- docker-compose-parse-file.yml : tool to parse a file.
## Instructions 
To build the image run :  ```docker build . -t clarity_test```
###Parse a file with file_name, init_date_time, end_date_time, host_name : 
- You have to use the docker-compose-parse-file.yml which have on the command section
the order to perform the action. 
I left <br/>
```"python src/cmd_tool.py --file_name input-file-10000.txt --time_init 1565647246869 --time_end 1565698945431 --host_name Sammiyah"``` 
<br/>as a test.

Run : ```docker-compose -f docker-compose-parse-file.yml run  --rm app``` 
to use the tool.

###Cron job tool 
On the first time I develop this function I set up a linux cron inside the docker container to 
perform the job every one hour. But I decided to remove it because I didn't want my docker
to be running all the time consuming resources.

I think the best way is to set up a cron on the host machine and run the docker application.
This functionality is configured on the ````docker-compose-cron-job.yml````
<br/> The full command on the cron is : ```0 * * * * docker-compose -f route/docker-compose-cron-job.yml run  --rm app```

###Anyway you can run the container and run the application from inside.

### Tests : 
Inside the container, on the root of the project /app/ run :
```python -m unittest -v test/unit/test_connection_file_unittest.py```

### About the streaming functionality :
I though to save the position of the last byte read with the file_descriptor.tell() function.
So the next time I'm running the job I don't need to start from the beginning, I just have 
to set the pointer to the last byte position visited the last time.

The problem is : ¿ Where do I save the byte position ? I could save it on the host. But I really didn't like the idea
to save it on a text file.
Then I also though about the possible collision between the cron process and the command line tool. So maybe I've to implement
some kind of mutex or lock to make it safer.
## To Improve / To Do 
- I tried to use env variables to configure the docker-compose-parse-file.yml but 
they are not working the same way from outside the container. There must be something with 
the args parsing on the code.
- Improve the write_results() function to receive the name of the file which the results are coming from, to be more
descriptive in order to write better logs. Or maybe to create a new result object with a file_name attribute.
- Maybe verify the configuration file ( Check the existence of the folders.. etc)
# Tools
- Docker version 19.03.5, build 633a0ea
- docker-compose version 1.25.4, build 8d51620a
- Docker source image : python:3.8-slim-buster

#UPDATE 2020
## Makefile
There is a new Makefile used for common operations such as run the tests, install dependencies or run the server.
- Make init = Run the server on port 8080.
- Make testing = Run the tests.
- Make install = Install dependencies. ( with no virtual environment )
## New Config Service
Now im using yaml files instead of ini. I just think that It's better on bigger applications, the tree structure is an advantage.
Anyway, what my new service does is scan every .yml file on the config folder and append the values to a dictionary, following this structure :

```{
    "cfg": {
            "value1": x,
            "value2": y
        }
    "anotherYmlFile": {
            "value99": d,
            "value77": p,
        } 
}
```

Now both Logging and Config services are Singleton since we will be using them everywhere.
## Server
In order to make this application more close to the real world there's a new way to return the results, through an API.
This application now need a Token to execute HTTP calls on the server.
The server is an EC2 instance running on my personal AWS cloud ( Free tier, lol )
The files that the application is reading are allocated on S3.
The files that are uploaded on the S3 bucket are the same placed on the /data folder

The ip is : http://3.23.95.43:8080/ ( hope It does not change )
## FastApi Server
Im using FastApi since I found it good for modeling responses and set dependencies on the calls.
We also can set the workers and threads in case we have a multicore cpu.

### Endpoints
There's only two end points at the moment.
- /token : ```curl -X POST "http://3.23.95.43:8080/token" -H  "accept: application/json" -H  "Content-Type: application/x-www-form-urlencoded" -d "grant_type=&username=admin&password=admin&scope=&client_id=&client_secret="```
The username and the password are : admin ( surprisingly secure )
- /connection : Where you can get a list of hostnames connected to the given host during the given period. ```curl -X POST "http://3.23.95.43:8080/connection?file_name=input-file-01-test.txt&time_init=1581932140&time_end=1581933040&host_name=fran" -H  "accept: application/json" -H  "Authorization: Bearer TOKEN_VALUE" -d ""```
Finally we have the /docs endpoint, where we can try out the application through front view on the navigator. 

## Issues
- I tried to set a CI/CD using cloud formation but I think it's too much effort right now since every one is using Jenkins or similar to create this kind of pipelines.
Anyway my Docker image is on the aws container registry but seems like I only can deploy the container directly using ECS rather than EC2.
- The server has not HTTPS connection, so It would be necessary to create a load balancer in order to use secure connections.
- The credentials to retrieve a token are not hashed, so It would be better to find a way to don't send them on a plain text.
- If the file is too big It's going to be impossible to open It on the server, unless we can download it by chunks.
I assume that each files only 10 mins of connections, and the traffic on the network is moderated.
## Future work
So I think that if we want to improve the time calculation for the given operations maybe we need to use a real data base.
Since we are asking questions related to the connections of a user It comes to my mind some kind of structure when u can have 
partitions by users, so you can ask directly on the user table.
I recently was reading about how MongoDB can perform this kind of partitions using what they call "Shard Key". This feature will divide
the physical partition in to a logical partitions using this key. So we can index directly the logical partition of a user and make the operation runs faster.
The jobs would be :

- Create the new models for the collection, and indexes.
- Select the best Shard key.
- Create methods for query the MongoDB.
- Decide if the application should manage the inserts on the MongoDB.