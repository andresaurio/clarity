import unittest
from src.domain.entity.connection import ConnectionFile
from src.service.config import Config
from typing import List
from src.domain.entity.connection import Connection


class TestConnectionFile(unittest.TestCase):
    def setUp(self):
        self.cfg = Config.get_instance()
        self.cnn = ConnectionFile()

    def test_evaluate_connection_to_empty(self):
        res = self.cnn.host_connections_to(self.cfg.get('cfg', 'empty_test_file'), 0, 0, 'name')
        self.assertEqual(len(res), 0)

    def test_evaluate_connection_to(self):
        cnn_list: List[Connection] = [
            Connection(1581932140, 'andrew', 'fran'),
            Connection(1581932320, 'ismael', 'fran'),
            Connection(1581932500, 'pedro', 'fran'),
            Connection(1581933040, 'ramon', 'fran')
        ]
        res = self.cnn.host_connections_to(self.cfg.get('cfg', 'sample_test_file'), 1581932140, 1581933040, 'fran')
        self.assertListEqual(cnn_list, res)

    def test_evaluate_connection_delay(self):
        cnn_list: List[Connection] = [
            Connection(1581933760, 'mario', 'jesus'),
            Connection(1581934480, 'ismael', 'jesus'),
            Connection(1581939134, 'ramon', 'jesus')
        ]
        res = self.cnn.host_connections_to(self.cfg.get('cfg', 'delay_test_file'), 1581933760, 1581939314, 'jesus')
        self.assertListEqual(cnn_list, res)

    def test_evaluate_connection_nonexistent_file(self):
        with self.assertRaises(Exception) as context:
            file_name = 'non_existent_file.txt'
            self.cnn.host_connections_to(self.cfg.get('cfg', file_name), 1581933760, 1581939314, 'jesus')
            self.assertTrue(
                f"The file {file_name} does not exits on the {self.cfg.get('ROUTE', 'data_route')} folder"
                in context.exception)

    def test_evaluate_connection_cron_job(self):
        cnn_list_to: List[Connection] = [
            Connection(1581933760, 'mario', 'jesus'),
            Connection(1581934480, 'ismael', 'jesus'),
        ]
        cnn_list_from: List[Connection] = [
            Connection(1581932320, 'ismael', 'fran'),
            Connection(1581932860, 'ismael', 'andrew'),
            Connection(1581934480, 'ismael', 'jesus'),

        ]
        res_host_to, res_host_from = self.cnn.host_connections_cron_job(
            file_name=self.cfg.get('cfg', 'sample_test_file'),
            init_time=1581932140,
            end_time=1581934480,
            host_to='jesus',
            host_from='ismael')

        self.assertListEqual(res_host_to, cnn_list_to) and self.assertListEqual(res_host_from, cnn_list_from)


if __name__ == "__main__":
    unittest.main()
